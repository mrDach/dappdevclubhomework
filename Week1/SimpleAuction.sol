pragma solidity ^0.4.20;

import "./SimpleAuctionInterface.sol";

contract SimpleAuction is AuctionInterface {
    struct Lot {
        string name;
        uint timestamp;
        address owner;
        uint price;
        uint minBid;
        uint lastBid; // price + bid
        address lastBidOwner;
        bool isEnded;
    }

    struct Owner {
        uint lotCounter;
        uint likes;
        uint dislikes;
    }

    mapping (uint => Lot) public lots;
    mapping (address => Owner) public owners;

    uint lotNonce = 0;

    function createLot(string _name, uint _price, uint _minBid) external returns (uint) {
        lotNonce++;

        lots[lotNonce] = Lot(_name, block.timestamp, msg.sender, _price, _minBid, 0, msg.sender, false);

        if (owners[msg.sender].lotCounter > 0) {
            owners[msg.sender] = Owner(1, 1, 1);
        } else {
            owners[msg.sender].lotCounter++;
        }
        return lotNonce;
    }

    function removeLot(uint _lotID) external isExistsLot(_lotID){
        delete lots[_lotID];
    }

    function bid(uint _lotID) external payable isExistsLot(_lotID) isProcessedLot(_lotID) returns (uint) {
        require(
            (msg.value >= (lots[_lotID].price + lots[_lotID].minBid)) &&
            (msg.value > (lots[_lotID].price + lots[_lotID].lastBid))
        );

        lots[_lotID].lastBidOwner.transfer(lots[_lotID].price + lots[_lotID].lastBid);

        uint prevBid = lots[_lotID].lastBid;

        lots[_lotID].lastBid = msg.value;
        lots[_lotID].lastBidOwner = msg.sender;

        return prevBid;
    }

    function processLot(uint _lotID) external payable isExistsLot(_lotID) isProcessedLot(_lotID) {
        require(msg.sender == lots[_lotID].owner);
        lots[_lotID].isEnded = true;
        lots[_lotID].owner.transfer(lots[_lotID].lastBid);
    }

    function getBidder(uint _lotID) external constant isExistsLot(_lotID) returns (address) {
        return lots[_lotID].lastBidOwner;
    }

    function isEnded(uint _lotID) external constant isExistsLot(_lotID)  returns (bool) {
        return lots[_lotID].isEnded == true;
    }

    function isProcessed(uint _lotID) external constant isExistsLot(_lotID) returns (bool) {
        return lots[_lotID].isEnded == false;
    }

    function exists(uint _lotID) external constant isExistsLot(_lotID) returns (bool) {
        return true;
    }

    function rate(uint _lotID, bool _option) external isExistsLot(_lotID) {
        require(
            (msg.sender == lots[_lotID].lastBidOwner) &&
            (lots[_lotID].isEnded)
        );

        if (_option) {
            owners[lots[_lotID].owner].likes++;
        } else {
            owners[lots[_lotID].owner].dislikes++;
        }
    }

    function getRating(address _owner) external constant returns (uint){
        require(owners[_owner].lotCounter > 0);

        return uint((owners[_owner].likes/owners[_owner].dislikes)+owners[_owner].lotCounter);
    }

    modifier isExistsLot(uint _lotID){
        require((_lotID <= lotNonce) && (lots[_lotID].timestamp > 0));
        _;
    }

    modifier isProcessedLot(uint _lotID){
        require(!lots[_lotID].isEnded);
        _;
    }
}
